﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AporteByronSantacruz3B
{
    public class Cliente
    {
        public string Nombre;
        private string Apellido;
        
        public Cliente()
        {
        }

        public string getNombre()
        {
            return Nombre;
        }
        public void setNombre(string nombre)
        {
            this.Nombre = nombre;
        }
        public string getApellidos()
        {
            return Apellido;
        }
        public void setApellido(string apellido)
        {
            this.Apellido = apellido;
        }
    }
}