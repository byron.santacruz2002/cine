﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AporteByronSantacruz3B
{
    class Program
    {
        static void Main(string[] args)
        {
            Cliente cliente = new Cliente();
            cliente.getNombre();
            cliente.setNombre("Byron Santacruz");
            Console.WriteLine("Cliente {0} se ha bienvenido", cliente.getNombre());
            Peliculas pelicula = new Peliculas();
            pelicula.Elegir();
            Productos_Comestribles productos_Comestribles = new Productos_Comestribles();
            productos_Comestribles.Escoger();
            Sala sala = new Sala();
            sala.getSala_asignada();
            sala.setSala_asignada(4);
            sala.getAsiento_asignado();
            sala.setAsiento_asignado(15);
            Console.WriteLine("Usted fue asiganado a la sala {0} su asiento es el numero {1} ", sala.getSala_asignada(), sala.getAsiento_asignado());
            Aperitivos aperitivos = new Aperitivos();
            aperitivos.Gratis();
            Console.WriteLine("Disfrute su pelicula");
            Console.ReadLine();
        }
    }
}
