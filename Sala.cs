﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AporteByronSantacruz3B
{
    public class Sala : Peliculas
    {
        private int Asiento_asignado;
        private int Sala_asignada;
        
        public int getAsiento_asignado()
        {
            return Asiento_asignado;
        }
        public void setAsiento_asignado(int asiento_asignado)
        {
            this.Asiento_asignado = asiento_asignado;
        }
        public int getSala_asignada()
        {
            return Sala_asignada;
        }
        public void setSala_asignada(int sala_asignada)
        {
            this.Sala_asignada = sala_asignada;
        }
    }
}